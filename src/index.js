// Reserved modules
const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const app = express();
const port = process.env.PORT || 8080;
require('dotenv').config();

// Custom modules
const {authRouter} = require('./controllers/authController');
const {userRouter} = require('./controllers/userController');
const {notesRouter} = require('./controllers/noteController');
const {authMiddleware} = require('./middlewares/authMiddleware');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', [authMiddleware], userRouter);
app.use('/api/notes', [authMiddleware], notesRouter);

const start = async () => {
  try {
    await mongoose.connect('mongodb+srv://todoList:todoList@cluster0.qsokx.mongodb.net/test', {
      useNewUrlParser: true, useUnifiedTopology: true,
    });

    app.listen(port);
  } catch (err) {
    console.log(`Error on server startup: ${err.message}`);
  }
};

start();

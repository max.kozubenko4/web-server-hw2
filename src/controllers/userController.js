const express = require('express');
const router = express.Router();

const {
  getUserProfileInfo,
  deleteUserProfile,
  updateUserProfilePassword,
} = require('../services/userService');

router.get('/me', async (req, res) => {
  const {userId} = req.user;

  try {
    const userInfo = await getUserProfileInfo(userId);

    console.log(userInfo);
    if (!userInfo) {
      res.status(400).json({message: 'string'});
    }

    const user = {};
    user._id = userInfo._id;
    user.username = userInfo.username;
    user.createdDate = userInfo.createdDate;

    res.status(200).json({user});
  } catch (err) {
    res.status(500).json({message: 'string'});
  }
});

router.delete('/me', async (req, res) => {
  const {userId} = req.user;

  try {
    await deleteUserProfile(userId);

    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.json({message: 'string'});
  }
});

router.patch('/me', async (req, res) => {
  const {userId} = req.user;
  const {newPassword} = req.body;

  if (!userId || !newPassword) {
    return res.status(400).json({message: 'string'});
  }

  try {
    await updateUserProfilePassword(userId, newPassword);
    res.json({message: 'Success'});
  } catch (err) {
    res.json({message: 'string'});
  }
});

module.exports = {
  userRouter: router,
};

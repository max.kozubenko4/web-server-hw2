const express = require('express');
const router = express.Router();

const {
  getAllNotes,
  createNote,
  getNote,
  updateNoteById,
  checkNoteById,
  deleteNoteById,
} = require('../services/notesServices');

router.get('/', async (req, res) => {
  const {userId} = req.user;

  if (!userId) {
    return res.status(400).json({message: 'string'});
  }

  // const limit = parseInt(req.query.limit);
  // const offset = parseInt(req.query.offset);

  // console.log(limit);
  // console.log(offset);

  try {
    const notes = await getAllNotes(userId);
    // const notes = resultNotes.slice(offset, offset + limit)
    // res.status(200).json({offset: offset, limit: limit, count: limit, notes});
    res.status(200).json({notes});
  } catch (err) {
    res.status(400).json({message: 'string'});
  }
});

router.post('/', async (req, res) => {
  const {userId} = req.user;
  const {text} = req.body;

  if (!userId || !text) {
    return res.status(400).json({message: 'string'});
  }

  try {
    await createNote(userId, text);
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: 'string'});
  }
});

router.get('/:id', async (req, res) => {
  const noteId = req.path.slice(1);
  const {userId} = req.user;

  if (!noteId || !userId) {
    return res.status(400).json({message: 'string'});
  }

  try {
    const note = await getNote(noteId, userId);
    res.status(200).json({note});
  } catch (err) {
    res.status(400).json({message: 'string'});
  }
});

router.put('/:id', async (req, res) => {
  const {userId} = req.user;
  const {text} = req.body;
  const noteId = req.path.slice(1);

  if (!noteId || !userId || !text) {
    return res.status(400).json({message: 'string'});
  }

  try {
    await updateNoteById(noteId, userId, text);
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: 'string'});
  }
});

router.patch('/:id', async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  if (!userId || !id) {
    return res.status(400).json({message: 'string'});
  }

  try {
    await checkNoteById(id, userId);
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: 'string'});
  }
});

router.delete('/:id', async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  if (!userId || !id) {
    return res.status(400).json({message: 'string'});
  }

  try {
    await deleteNoteById(id, userId);
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: 'string'});
  }
});

module.exports = {
  notesRouter: router,
};

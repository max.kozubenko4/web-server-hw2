const {User} = require('../models/userModel');

const getUserProfileInfo = async (userId) => {
  const user = await User.findOne({_id: userId});
  return user;
};

const deleteUserProfile = async (userId) => {
  await User.findOneAndRemove({_id: userId});
};

const updateUserProfilePassword = async (userId, newPassword) => {
  const update = {password: newPassword};
  await User.findOneAndUpdate({_id: userId}, update);
};

module.exports = {
  getUserProfileInfo,
  deleteUserProfile,
  updateUserProfilePassword,
};
